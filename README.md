# Space Pizza (Backend)

> First Pizza delivery from the low orbit!

## About this project

This project is build for development purposes only, as test task for Innoscripta. Sadly enought, no pizza from space, my friend 😔

### Technologies used

- Fresh and shiny Laravel 8
- Generated OpenAPI 3.0 (Former Swagger) via PHPDoc Annotations
- JWT Auth

### Fun facts

- It was a rapid development, so there is a bunch of *bad things*
- I love, that Taylor finaly decided to move Models to separate namespace
- No real money convertion. All prices are random. Mozarella can cost 0.07 USD and 9 BTC at the same time. Somebody from 2010 is still crying
- I'm kinda bored of writing OpenAPI for all the test taks. But if you suspect that it is not finished, you are wrong. It just typed with transparent color and font-size equals zero.
- I prefer PostgreSQL over MySQL

## Clarifications

1. **Why Order does not stored by relations to products?**    
    That's simple enough! As time changes, product prices might change ether, but we don't want to change anything in user's history, right? That's why Order keeps a dump of it's products
2. All Fun facts are real. If only they are not.
3. OpenAPI is accesible via `/api/documentation`

## Build Setup

### Prerequesits

- PHP 7.4
- MySQL

### Startup script

```bash
# install dependencies
$ composer install

$ php artisan key:generate
$ php artisan jwt:secret
$ php artisan migrate:fresh --seed

$ valet link # assuming you have Valet installe. Otherwise, it's your problem now ¯\_(ツ)_/¯
```
