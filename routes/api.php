<?php

use App\Http\Controllers\AddressController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\PizzaController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResource('pizzas', PizzaController::class)->only(['index', 'show']);
Route::apiResource('orders', OrderController::class)->only(['index', 'store']);
Route::apiResource('addresses', AddressController::class)->only(['index']);

Route::group([
    'prefix' => 'auth',
    'as' => 'auth.',
    'namespace' => 'Auth',
], function (Router $router) {
    $router->post('register', [AuthController::class, 'register'])->name('register')->middleware(['throttle:10,5']);
    $router->post('token', [AuthController::class, 'token'])->name('token')->middleware(['throttle:10,5']);

    $router->group([
        'middleware' => ['auth:api'],
    ], function (Router $router) {
        $router->get('me', [AuthController::class, 'me'])->name('token.me');
        $router->post('refresh', [AuthController::class, 'refresh'])->name('token.refresh');
        $router->delete('token', [AuthController::class, 'logout'])->name('token.logout');
    });
});
