{
    "openapi": "3.0.0",
    "info": {
        "title": "L5 OpenApi",
        "description": "Test Task OpenAPI",
        "version": "1.0.0"
    },
    "servers": [
        {
            "url": "http://pizza-task-backend.test/api/",
            "description": "Valet Server"
        }
    ],
    "paths": {
        "/v1/auth/register": {
            "post": {
                "tags": [
                    "auth"
                ],
                "summary": "Create new User and get a JWT.",
                "operationId": "auth.register",
                "requestBody": {
                    "content": {
                        "application/x-www-form-urlencoded": {
                            "schema": {
                                "$ref": "#/components/schemas/RegistrationRequest"
                            }
                        },
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/RegistrationRequest"
                            }
                        }
                    }
                },
                "responses": {
                    "200": {
                        "$ref": "#/components/responses/TokenResponse"
                    },
                    "401": {
                        "description": "Unauthorized login.",
                        "content": {
                            "application/json": {}
                        }
                    }
                }
            }
        },
        "/v1/auth/token": {
            "post": {
                "tags": [
                    "auth"
                ],
                "summary": "Get a JWT via given credentials.",
                "operationId": "auth.token",
                "requestBody": {
                    "content": {
                        "application/x-www-form-urlencoded": {
                            "schema": {
                                "$ref": "#/components/schemas/LoginRequest"
                            }
                        },
                        "application/json": {
                            "schema": {
                                "$ref": "#/components/schemas/LoginRequest"
                            }
                        }
                    }
                },
                "responses": {
                    "401": {
                        "$ref": "#/components/responses/UnauthorizedError"
                    }
                }
            },
            "delete": {
                "tags": [
                    "auth"
                ],
                "summary": "Log the user out (Invalidate the token).",
                "operationId": "auth.token.logout",
                "responses": {
                    "204": {
                        "description": "Token successfully invalidated"
                    },
                    "401": {
                        "$ref": "#/components/responses/UnauthorizedError"
                    }
                },
                "security": [
                    {
                        "bearerAuth": []
                    }
                ]
            }
        },
        "/v1/auth/refresh": {
            "post": {
                "tags": [
                    "auth"
                ],
                "summary": "Refreshes a JWT (ie. extends it's TTL)",
                "operationId": "auth.token.refresh",
                "responses": {
                    "200": {
                        "$ref": "#/components/responses/TokenResponse"
                    },
                    "401": {
                        "$ref": "#/components/responses/UnauthorizedError"
                    }
                },
                "security": [
                    {
                        "bearerAuth": []
                    }
                ]
            }
        },
        "/pizzas": {
            "get": {
                "summary": "Display a listing of the resource.",
                "operationId": "pizzas.index",
                "responses": {
                    "200": {
                        "$ref": "#/components/responses/PizzaResourceCollection"
                    }
                }
            }
        },
        "/pizzas/{id}": {
            "get": {
                "summary": "Display the specified resource.",
                "operationId": "pizzas.show",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "schema": {
                            "type": "integer",
                            "format": "int64"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "$ref": "#/components/responses/PizzaResource"
                    }
                }
            }
        }
    },
    "components": {
        "schemas": {
            "LoginRequest": {
                "title": "Login authentication request",
                "required": [
                    "email",
                    "password"
                ],
                "properties": {
                    "email": {
                        "title": "Email",
                        "format": "email",
                        "example": "admin@example.com"
                    },
                    "password": {
                        "title": "Password",
                        "format": "password",
                        "example": "secret"
                    }
                },
                "type": "object"
            },
            "RegistrationRequest": {
                "title": "Registration request",
                "required": [
                    "email",
                    "login",
                    "password",
                    "password_confirmation"
                ],
                "properties": {
                    "email": {
                        "title": "Email",
                        "format": "email",
                        "example": "admin@example.com"
                    },
                    "password": {
                        "title": "Password",
                        "format": "password",
                        "example": "secret"
                    },
                    "password_confirmation": {
                        "title": "Password (Confirmation)",
                        "format": "password",
                        "example": "secret"
                    },
                    "policy": {
                        "title": "Policy",
                        "example": "true"
                    }
                },
                "type": "object"
            },
            "Pizza": {
                "title": "Pizza Model",
                "properties": {
                    "title": {
                        "title": "Title",
                        "example": "Peperoni"
                    },
                    "image": {
                        "title": "Image URL"
                    },
                    "price_base": {
                        "title": "Base price",
                        "example": "10.12"
                    }
                },
                "type": "object"
            }
        },
        "responses": {
            "TokenResponse": {
                "description": "JWT Token Response",
                "content": {
                    "application/json": {
                        "schema": {
                            "properties": {
                                "access_token": {
                                    "description": "Get the token array structure.",
                                    "format": "jwt",
                                    "example": "xxxxx.yyyyy.zzzzz"
                                },
                                "token_type": {
                                    "description": "Get the token array structure.",
                                    "example": "bearer"
                                },
                                "expires_in": {
                                    "description": "Get the token array structure.",
                                    "format": "int32",
                                    "example": "9600"
                                }
                            },
                            "type": "object"
                        }
                    }
                }
            },
            "UnauthorizedError": {
                "description": "Access token is missing or invalid",
                "content": {
                    "application/json": {}
                }
            },
            "PizzaResource": {
                "description": "Pizza Resource Response",
                "content": {
                    "application/json": {
                        "schema": {
                            "properties": {
                                "data": {
                                    "type": "object"
                                }
                            },
                            "type": "object"
                        }
                    }
                }
            },
            "PizzaResourceCollection": {
                "description": "Pizza Resource Response",
                "content": {
                    "application/json": {
                        "schema": {
                            "properties": {
                                "data": {
                                    "type": "array",
                                    "items": {}
                                }
                            },
                            "type": "object"
                        }
                    }
                }
            }
        }
    },
    "security": [
        []
    ]
}