<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Pizza Model",
 *     type="object",
 * )
 */
class Order extends Model
{
    use HasFactory;

    public const METHOD_CASH = 'CASH';
    public const PAYMENT_METHODS = [self::METHOD_CASH];

    protected $casts = [
        'items' => 'array',
    ];

    protected $fillable = [
        'name',
        'email',
        'currency',
        'items',
        'total',
        'method',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function getNumberAttribute()
    {
        return "{$this->created_at->year}-{$this->id}";
    }

    public function toArray()
    {
        return [
            'address' => $this->address,
            'number' => $this->number,
            'name' => $this->name,
            'email' => $this->email,
            'items' => $this->items,
            'currency' => $this->currency,
            'total' => $this->total,
            'method' => $this->method,
        ];
    }
}
