<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @OA\Schema(
 *     title="Pizza Model",
 *     type="object",
 *     @OA\Property(
 *         property="title",
 *         title="Title",
 *         example="Peperoni",
 *         @OA\Schema(type="text")
 *     ),
 *     @OA\Property(
 *         property="descsription",
 *         title="Short description",
 *         example="Lorem ipsum dolar sit amet",
 *         @OA\Schema(type="textarea")
 *     ),
 *     @OA\Property(
 *         property="image",
 *         title="Image URL",
 *         @OA\Schema(type="number"),
 *     ),
 *     @OA\Property(
 *         property="price_base",
 *         title="Base price",
 *         example="10.12",
 *         @OA\Schema(type="number"),
 *     ),
 * )
 */
class Pizza extends Model
{
    use HasFactory;

    public function prices()
    {
        return $this->morphMany(Price::class, 'priceable');
    }

    public function getPriceFor($currency)
    {
        return $this->prices->where('currency', $currency)->first();
    }

    public function toArray()
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'image' => $this->image,
            'prices' => $this->prices->mapWithKeys(fn(Price $price) => [
                $price->currency => $price->toArray()
            ])
        ];
    }
}
