<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    use HasFactory;

    public const CURRENCIES = [ 'EUR', 'USD' ];

    public function priceable()
    {
        return $this->morphTo();
    }

    public function toArray()
    {
        return [
            'currency' => $this->currency,
            'price' => $this->price,
        ];
    }
}
