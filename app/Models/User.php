<?php

namespace App\Models;

use Hash;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function boot()
    {
        parent::boot();

        static::saving(function (self $user) {
            // Hash user password, if not already hashed
            if (Hash::needsRehash($user->password)) {
                $user->password = Hash::make($user->password);
            }
        });
    }

    /**
     * ! Normal relations does not support (any-key OR) relations
     *
     * We are lacking eager loading, but whatever...
     *
     * @return Order[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getOrdersAttribute() {
        return Order::query()
            ->where('user_id', $this->id)
            ->orWhere('email', $this->email)
            ->orderByDesc('created_at')
            ->get();
    }

    public function getAddressesAttribute() {
        return $this->orders
            ->filter(fn (Order $order) => !empty($order->address))
            ->pluck('address')
            ->unique();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [
            'name',
            'email',
        ];
    }
}
