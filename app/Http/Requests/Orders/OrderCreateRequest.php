<?php

namespace App\Http\Requests\Orders;

use App\Models\Order;
use App\Models\Price;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

/**
 * @OA\Schema(
 *     title="New Order request",
 *     type="object",
 */
class OrderCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'data.name' => [
                'bail',
                'sometimes',
                'nullable',
                Rule::requiredIf(auth()->guest()),
                'max:255',
            ],
            'data.email' => [
                'bail',
                'sometimes',
                'nullable',
                Rule::requiredIf(auth()->guest()),
                'email',
            ],

            'data.paymentMethod' => [
                'required',
                Rule::in(Order::PAYMENT_METHODS),
            ],
            'data.currency' => [
                'required',
                Rule::in(Price::CURRENCIES),
            ],

            'data.items' => [
                'required',
                'array',
            ],
            'data.items.*.id' => [
                'required',
                'exists:pizzas',
            ],
            'data.items.*.qty' => [
                'required',
                'integer',
                'min:0'
            ],

            'data.address.id' => [
                'sometimes',
                'exists:addresses,id'
            ],
            'data.address.country' => [
                'required_without:data.address.id',
                'alpha_num',
            ],
            'data.address.region' => [
                'required_without:data.address.id',
                'alpha_num',
            ],
            'data.address.city' => [
                'required_without:data.address.id',
                'alpha_num',
            ],
            'data.address.street_address' => [
                'required_without:data.address.id',
                'max:512',
            ],
        ];
    }
}
