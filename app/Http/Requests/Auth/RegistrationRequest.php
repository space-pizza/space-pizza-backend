<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Registration request",
 *     type="object",
 *     required={"email", "login", "password", "password_confirmation"},
 *     @OA\Property(
 *         property="email",
 *         title="Email",
 *         format="email",
 *         example="admin@example.com",
 *         @OA\Schema(type="text")
 *     ),
 *     @OA\Property(
 *         property="password",
 *         title="Password",
 *         format="password",
 *         example="secret",
 *         @OA\Schema(type="text")
 *     ),
 *     @OA\Property(
 *         property="password_confirmation",
 *         title="Password (Confirmation)",
 *         format="password",
 *         example="secret",
 *         @OA\Schema(type="text")
 *     ),
 *     @OA\Property(
 *         property="policy",
 *         title="Policy",
 *         example="true",
 *         @OA\Schema(type="boolean")
 *     ),
 * )
 */
class RegistrationRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->guest();
    }

    public function rules()
    {
        return [
            'name' => ['required', 'max:255'],
            'email' => ['required', 'email', Rule::unique('users', 'email')],
            'password' => ['required', 'confirmed', /* 'pwned' */],
            'policy' => ['required', 'accepted']
        ];
    }
}
