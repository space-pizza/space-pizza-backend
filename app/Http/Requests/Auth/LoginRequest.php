<?php

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     title="Login authentication request",
 *     type="object",
 *     required={"email", "password"},
 *     @OA\Property(
 *         property="email",
 *         title="Email",
 *         format="email",
 *         example="admin@example.com",
 *         @OA\Schema(type="text")
 *     ),
 *     @OA\Property(
 *         property="password",
 *         title="Password",
 *         format="password",
 *         example="secret",
 *         @OA\Schema(type="text")
 *     )
 * )
 */
class LoginRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->guest();
    }

    public function rules()
    {
        return [
            'email' => ['required', 'email'],
            'password' => ['required']
        ];
    }
}
