<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

/**
 * @OA\Response(
 *     response="PizzaResourceCollection",
 *     description="Pizza Resource Response",
 *     @OA\JsonContent(
 *         @OA\Property(
 *             property="data",
 *             type="array",
 *             @OA\Items(
 *                 @OA\Schema(ref="#/components/schemas/Pizza"),
 *             ),
 *         ),
 *     ),
 * )
 */
class PizzaResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
