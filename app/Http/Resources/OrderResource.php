<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @OA\Response(
 *     response="OrderResource",
 *     description="Order Resource Response",
 *     @OA\JsonContent(
 *         @OA\Property(
 *             property="data",
 *             type="object",
 *             @OA\Schema(ref="#/components/schemas/Order"),
 *         ),
 *     ),
 * )
 */
class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
