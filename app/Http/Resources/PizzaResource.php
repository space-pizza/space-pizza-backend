<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use OpenApi\Annotations as OA;

/**
 * @OA\Response(
 *     response="PizzaResource",
 *     description="Pizza Resource Response",
 *     @OA\JsonContent(
 *         @OA\Property(
 *             property="data",
 *             type="object",
 *             @OA\Schema(ref="#/components/schemas/Pizza"),
 *         ),
 *     ),
 * )
 */
class PizzaResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
