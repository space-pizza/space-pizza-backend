<?php

namespace App\Http\Controllers;

use App\Http\Resources\AddressResource;
use App\Http\Resources\AddressResourceCollection;
use App\Models\User;
class AddressController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('index');
    }

    public function index()
    {
        /** @var User $user */
        $user = auth()->user();
        $addresses = $user->addresses;

        return (AddressResource::collection(new AddressResourceCollection($addresses)))->response();
    }
}
