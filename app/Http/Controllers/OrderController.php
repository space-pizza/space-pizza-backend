<?php

namespace App\Http\Controllers;

use App\Http\Requests\Orders\OrderCreateRequest;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderResourceCollection;
use App\Models\Address;
use App\Models\Order;
use App\Models\Pizza;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->only('index');
    }

    public function index()
    {
        /** @var User $user */
        $user = auth()->user();
        $orders = $user->orders;

        return (OrderResource::collection(new OrderResourceCollection($orders)))->response();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @OA\Post(
     *     path="/orders",
     *     operationId="orders.store",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/OrderCreateRequest"),
     *         ),
     *         @OA\JsonContent(ref="#/components/schemas/OrderCreateRequest"),
     *     ),
     *     @OA\Response(response="201", ref="#/components/responses/OrderResource"),
     * )
     *
     * @param OrderCreateRequest $request
     * @return Response
     */
    public function store(OrderCreateRequest $request)
    {
        $data = $request->validated()['data'];

        $items = collect($data['items'])->map(fn($item) => [
            'item' => Pizza::findOrFail($item['id']),
            'qty' => $item['qty']
        ]);
        $subtotal = $items
            ->map(fn($item) => $item['item']->getPriceFor($data['currency'])->price * $item['qty'])
            ->sum();

        if ($request->has('data.address.id')) {
            $address = Address::findOrFail($data['address']['id']);
        } else {
            $address = Address::firstOrCreate([
                'country' => $data['address']['country'],
                'region' => $data['address']['region'],
                'city' => $data['address']['city'],
                'street_address' => $data['address']['street_address'],
            ]);
        }

        $order = Order::make([
            'name' => $data['name'] ?? auth()->user()->name,
            'email' => $data['email'] ?? auth()->user()->email,
            'currency' => $data['currency'],
            'method' => $data['paymentMethod'],
            'items' => $items,
            'total' => round($subtotal * 1.3 + 5, 2), // VAT + delivery
        ]);
        $order->user()->associate(auth()->user());
        $order->address()->associate($address);
        $order->save();

        return (new OrderResource($order))->response(Response::HTTP_CREATED);
    }
}
