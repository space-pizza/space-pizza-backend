<?php

namespace App\Http\Controllers;

use App\Http\Resources\PizzaResource;
use App\Http\Resources\PizzaResourceCollection;
use App\Models\Pizza;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;

class PizzaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @OA\Get(
     *     path="/pizzas",
     *     operationId="pizzas.index",
     *     @OA\Response(response="200", ref="#/components/responses/PizzaResourceCollection"),
     * )
     *
     * @return Response
     */
    public function index()
    {
        return (PizzaResource::collection(new PizzaResourceCollection(Pizza::all())))->response();
    }

    /**
     * Display the specified resource.
     *
     * @OA\Get(
     *     path="/pizzas/{id}",
     *     operationId="pizzas.show",
     *     @OA\Parameter(name="id", in="path", @OA\Schema(type="integer", format="int64")),
     *     @OA\Response(response="200", ref="#/components/responses/PizzaResource"),
     * )
     *
     * @param Pizza $order
     * @return Response
     */
    public function show(Pizza $pizza)
    {
        return (new PizzaResource($pizza))->response();
    }
}
