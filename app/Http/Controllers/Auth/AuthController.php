<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\RegistrationRequest;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\Response;

final class AuthController extends Controller
{

    /**
     * Create new User and get a JWT.
     *
     * @OA\Post(
     *     path="/v1/auth/register",
     *     tags={"auth"},
     *     operationId="auth.register",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/RegistrationRequest"),
     *         ),
     *         @OA\JsonContent(ref="#/components/schemas/RegistrationRequest"),
     *     ),
     *     @OA\Response(response="200", ref="#/components/responses/TokenResponse"),
     *     @OA\Response(
     *         response=401,
     *         description="Unauthorized login.",
     *         @OA\MediaType(mediaType="application/json"),
     *     ),
     * )
     *
     * @param \Auctions\Modules\Users\Http\Requests\RegistrationRequest $request
     *
     * @throws AuthenticationException
     *
     * @return Response
     */
    public function register(RegistrationRequest $request): Response
    {
        $user = User::create($request->validated());

        $token = auth()->login($user);

        return $this->respondWithToken($token)->setStatusCode(Response::HTTP_CREATED);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @OA\Post(
     *     path="/v1/auth/token",
     *     tags={"auth"},
     *     operationId="auth.token",
     *     @OA\RequestBody(
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(ref="#/components/schemas/LoginRequest"),
     *         ),
     *         @OA\JsonContent(ref="#/components/schemas/LoginRequest"),
     *     ),
     *
     *     @OA\Response(response="401", ref="#/components/responses/UnauthorizedError"),
     * )
     *
     * @param \Auctions\Modules\Users\Http\Requests\LoginRequest $request
     *
     * @throws AuthenticationException
     *
     * @return Response
     */
    public function token(LoginRequest $request): Response
    {
        if (!$token = auth()->attempt($this->getCredentials($request))) {
            throw new AuthenticationException('Unauthorized login.');
        }

        return $this->respondWithToken($token);
    }

    /**
     * Refreshes a JWT (ie. extends it's TTL)
     *
     * @OA\Post(
     *     path="/v1/auth/refresh",
     *     tags={"auth"},
     *     operationId="auth.token.refresh",
     *     @OA\Response(response="200", ref="#/components/responses/TokenResponse"),
     *     @OA\Response(response="401", ref="#/components/responses/UnauthorizedError"),
     *     security={
     *         {"bearerAuth": {}}
     *     },
     * )
     *
     * @return Response
     */
    public function refresh(): Response
    {
        return $this->respondWithToken(auth()->refresh());
    }

    public function me(): Response
    {
        /** @var User $user */
        $user = auth()->user();

        return response()->json([
            'data' => $user->toArray(),
        ]);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @OA\Delete(
     *     path="/v1/auth/token",
     *     tags={"auth"},
     *     operationId="auth.token.logout",
     *     @OA\Response(response="204", description="Token successfully invalidated"),
     *     @OA\Response(response="401", ref="#/components/responses/UnauthorizedError"),
     *     security={
     *         {"bearerAuth": {}}
     *     },
     * )
     *
     * @return Response
     */
    public function logout(): Response
    {
        auth()->logout();

        return response()->noContent();
    }

    /**
     * Get the token array structure.
     *
     * @OA\Response(
     *     response="TokenResponse",
     *     description="JWT Token Response",
     *     @OA\MediaType(
     *         mediaType="application/json",
     *         @OA\Schema(
     *             type="object",
     *             @OA\Property(
     *                 property="access_token",
     *                 format="jwt",
     *                 example="xxxxx.yyyyy.zzzzz",
     *                 @OA\Schema(type="text"),
     *             ),
     *             @OA\Property(
     *                 property="token_type",
     *                 example="bearer",
     *                 @OA\Schema(type="text"),
     *             ),
     *             @OA\Property(
     *                 property="expires_in",
     *                 format="int32",
     *                 example="9600",
     *                 @OA\Schema(type="number"),
     *             ),
     *         ),
     *     ),
     * )
     *
     * @param string $token
     *
     * @return Response
     */
    protected function respondWithToken($token): Response
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }

    /**
     * @param LoginRequest $request
     *
     * @throws AuthenticationException
     *
     * @return array
     */
    protected function getCredentials(LoginRequest $request): array
    {
        // Get for Auth Basic
        if ($authHeader = $request->header('Authorization')) {
            if (strtolower(substr(base64_decode(trim(substr($authHeader, 5))), 0, 5)) !== 'basic') {
                throw new AuthenticationException('Invalid authorization header, should be type basic');
            }

            return collect(['login', 'password'])
                ->combine(explode(':', base64_decode(trim(substr($authHeader, 5))), 2))
                ->all();
        }

        return $request->validated();
    }
}
