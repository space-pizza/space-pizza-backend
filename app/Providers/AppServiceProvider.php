<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Schema;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="L5 OpenApi",
 *      description="Test Task OpenAPI",
 * )
 *
 * @OA\Server(
 *      url="http://pizza-task-backend.test/api/",
 *      description="Valet Server"
 *  )
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
