<?php

namespace Database\Seeders;

use App\Models\Pizza;
use App\Models\Price;
use Illuminate\Database\Seeder;

class PizzaSeeder extends Seeder
{
    protected const PIZZAS = [
        [
            'title' => 'Peperoni',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/4df905b0-0a43-4e95-a096-8e470918a267.jpg'
        ],
        [
            'title' => 'Cheese',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/c2da53ec-00e2-4446-a4e6-74b83ed0b357.jpg'
        ],
        [
            'title' => 'Margarita',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/21379561a76646f1945f1bc759c749b5_292x292.jpeg'
        ],
        [
            'title' => 'Hawaiian',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/b952eb17-77b8-4a14-b982-42fbf5ceaf0e.jpg'
        ],
        [
            'title' => 'Double Peperoni',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/accc2ec9-5a93-4fb4-94bf-9006ce23fede.jpg'
        ],
        [
            'title' => 'Meat Lover\'s',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/b61cca95-caa6-4952-94b2-6896098b4f53.jpg'
        ],
        [
            'title' => 'Italian',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/ac08a6eb-a136-4e76-83bc-bdd5253ff123.jpg'
        ],
        [
            'title' => 'Four Cheese',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/baf084f4-fc94-443c-a646-fdeff9f301a6.jpg'
        ],
        [
            'title' => 'Mexican',
            'image' => 'https://cdn.dodostatic.net/static/Img/Products/Pizza/ru-RU/ecd9d5b3-0cfc-4138-9559-18d9631fe8aa.jpg'
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $currencies = collect(Price::CURRENCIES)
            ->map(fn ($currency) => [ 'currency' => $currency ]);

        foreach (self::PIZZAS as $pizza) {
            /** @var Pizza $pizza */
            $pizza = Pizza::factory()->create($pizza);
            $currencies->map(fn ($attributes) => $pizza->prices()->save(Price::factory()->makeOne($attributes)));
        }
    }
}
